# content-scripts-v1

python scripts to collect and process info

- `best_x_x/` folder for scrapy files
- `extractSurfer.py` for extracting surfer keywords and guidelines into JSON
- `search.py` to search from [Google](https://www.google.com) using [Custom Search JSON API](https://developers.google.com/custom-search/v1/overview)