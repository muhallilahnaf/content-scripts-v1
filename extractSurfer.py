import re
import json
import os

with open(f'{os.getcwd()}/surfer-guidelines.txt', 'r') as f1:
    
    lines = f1.readlines()
    print('lines read')

    kwDict = dict()

    lineWordCount = lines[ lines.index('## CONTENT STRUCTURE\n') + 1 ]
    kwDict['wordCount'] = re.findall(r'\d+', lineWordCount)

    keywordsStart = lines.index('## IMPORTANT TERMS TO USE\n') + 2
    keywordsEnd = lines.index('## OTHER RELEVANT TERMS\n') - 2

    keywords = dict()
    for i in range(keywordsStart, keywordsEnd + 1):
        line = lines[i]
        parts = re.split(r':', line)
        keyword = parts[0].replace('* ', '')
        limit = re.findall(r'\d+', parts[1])
        keywords[keyword] = limit

    kwDict['keywords'] = keywords

with open(f'{os.getcwd()}/keywords.json', 'w') as f2:
    f2.write(json.dumps(kwDict, indent=4))
    print('file saved: keywords.json')
    