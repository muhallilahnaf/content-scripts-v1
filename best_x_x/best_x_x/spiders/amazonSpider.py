import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import amazonItems

class amazonSpider(scrapy.Spider):
    name = "amazon"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 10,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['amazon.com']
    start_urls = [
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        loader = ItemLoader(item=amazonItems())
        
        title = response.css('#productTitle::text').get()

        features = response.css('#feature-bullets li *::text').getall()
        description = response.css('#productDescription *::text').getall()
        details = response.css('#prodDetails tr *::text').getall()

        reviews = response.css('#cm-cr-dp-review-list .review')

        reviewList = list()
        for review in reviews:
            rating = r.css('.a-row > a[title]').attrib['title']
            reviewTitle = r.css('.a-row > a[data-hook="review-title"] *::text').getall()
            variant = r.css('[data-hook="format-strip-linkless"] *::text').getall()
            body = r.css('[data-hook="review-collapsed"] *::text').getall()

        #     reviewList.append(spec.css('#label::text').get())


        # loader.add_value("title", title)
        # loader.add_value("subtitle", subtitle)
        # loader.add_value("price", price)
        # loader.add_value("specs", specs)
        # loader.add_value("overview", overview)

        loadedItems = loader.load_item()

        yield loadedItems
