import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import tomsradarItems

class pcgamerSpider(scrapy.Spider):
    name = "pcgamer"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 10,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['pcgamer.com']
    start_urls = [
        'https://www.pcgamer.com/corsair-vengeance-6182-gaming-desktop-review/',
        'https://www.pcgamer.com/corsair-k100-gaming-keyboard-review/'
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        loader = ItemLoader(item=tomsradarItems())
        
        review = response.css('.review-article')

        title = review.css('h1::text').get()
        subtitle = review.css('h2::text').get()

        section = review.css('.content-wrapper')

        verdict = section.css('.verdict-boxout p.game-verdict::text').get()

        procon = section.css('.pro-con ul')
        pros = procon[0].css('li::text').getall()
        cons = procon[1].css('li::text').getall()

        article = section.css('#article-body > *:not(aside):not(figure):not(table):not([class*="fancy-box"]) *::text').getall()
        table = section.css('#article-body .fancy-box *::text').getall()

        loader.add_value("title", title)
        loader.add_value("subtitle", subtitle)
        loader.add_value("verdict", verdict)
        loader.add_value("pros", pros)
        loader.add_value("cons", cons)
        loader.add_value("table", table)
        loader.add_value("article", article)

        loadedItems = loader.load_item()

        yield loadedItems
