import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import tomsradarItems

class tomsradarSpider(scrapy.Spider):
    name = "tomsradar"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 5,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['tomsguide.com', 'techradar.com', 'tomshardware.com']
    start_urls = [
        'https://www.tomsguide.com/us/corsair-glaive-rgb-pro,review-6411.html',
        'https://www.techradar.com/reviews/corsair-hs70bt',
        'https://www.tomshardware.com/reviews/corsair-k70-rgb-mk-2-low-profile-gaming-keyboard,6128.html'
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        loader = ItemLoader(item=tomsradarItems())
        
        review = response.css('.review-article')

        title = review.css('h1::text').get()
        subtitle = review.css('h2::text').get()

        section = review.css('.content-wrapper')

        verdict = section.css('.verdict p::text').get()

        procon = section.css('.pro-con ul')
        pros = procon[0].css('li::text').getall()
        cons = procon[1].css('li::text').getall()

        article = section.css('#article-body > *:not(aside):not(figure):not(table) *::text').getall()
        table = section.css('#article-body table *::text').getall()




        # ign
        # title = response.css('.article-headline h1::text').get()
        # subtitle = response.css('.article-sub-headline h2::text').get()
        # verdict = response.css('.article-section.verdict *::text').get()
        # article = response.css('.article-page > *:not([class*="widget-container"]::text).getall()


        # trustedreviews // fix
        # title = response.css('header h1::text').get()
        # subtitle = response.css('header div.review__intro::text').get()
        # section = response.css('.articleBody')
        # verdict = section.css('.review-points > *:not([class*="review-points__pros"]):not([class*="review-points__cons"])::text').getall()
        # pros = section.css('.review-points__pros li *::text').getall()
        # cons = section.css('.review-points__cons li *::text').getall()
        # specs = section.css('.review-features li *::text').getall()
        # article = section.css('.articleBody > *:not([class*="review-points"]):not([class*="review-features"]):not([id*="mc_embed_signup"]):not([class*="review-score"]):not([class*="post-author-card"])::text').getall()
        # // fix article

        # guru3d
        # actual_link = 'https://www.guru3d.com/' + response.css('.pagelink:last-child a').attrib['href']
        # then
        # title = response.css('h1::text').getall()
        # article = response.css('span[itemprop*="articleBody"] *:not(ul)::text').getall()

        # windowscentral //checked
        # title = response.css('header h1::text').get()
        # subtitle = response.css('header .article-header__intro::text').get()
        # verdict = response.css('.article-body__r3c0-bl0ck-description > p::text').getall()[1]
        # procon = response.css('.article-body__good-bad ul')
        # pros = procon[0].css('li *::text').getall()
        # cons = procon[1].css('li *::text').getall()
        # article = response.css('div.article-body > .article-body__section--narrow > *:not(blockquote):not(div)::text').getall()
        # table = response.css('div.article-body table *::text').getall() // could be improved


        loader.add_value("title", title)
        loader.add_value("subtitle", subtitle)
        loader.add_value("verdict", verdict)
        loader.add_value("pros", pros)
        loader.add_value("cons", cons)
        loader.add_value("table", table)
        loader.add_value("article", article)

        loadedItems = loader.load_item()

        yield loadedItems

        # [V] Techradar
        # [V] Tom's guide
        # [V] Pcmag
        # [V] Pcgamer
        # [X] Ign 403
        # [V] Techpowerup
        # [V] Windowscentral
        # [V] Trustedreviews
        # [V] Guru3d
        # [ ] Wepc
        # [ ] Businessinsider
        # [ ] Gamesradar
        # [ ] Gamingscan
