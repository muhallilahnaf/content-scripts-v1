import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import tomsradarItems

class pcmagSpider(scrapy.Spider):
    name = "pcmag"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 10,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['pcmag.com']
    start_urls = [
        'https://www.pcmag.com/reviews/corsair-dark-core-rgb-pro-se',
        'https://www.pcmag.com/reviews/corsair-vengeance-i7200',
        'https://www.pcmag.com/reviews/corsair-gaming-k65-rgb'
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        loader = ItemLoader(item=tomsradarItems())
        
        header = response.css('#content-header')

        title = header.css('h1::text').get()
        subtitle = header.css('h1 + p::text').get()

        verdict = response.css('.bottom-line *::text').get()

        procon = response.css('.pros-cons ul')
        pros = procon[0].css('li::text').getall()
        cons = procon[1].css('li::text').getall()

        # *:not(aside):not(img):not([id*="similar-products"]):not([class*="review-card"]):not([id*="comments"])
        article = response.css('#article > *:not(aside):not(img):not(div) *::text').getall()

        table = response.css('#article table *::text').getall()

        loader.add_value("title", title)
        loader.add_value("subtitle", subtitle)
        loader.add_value("verdict", verdict)
        loader.add_value("pros", pros)
        loader.add_value("cons", cons)
        loader.add_value("table", table)
        loader.add_value("article", article)

        loadedItems = loader.load_item()

        yield loadedItems
