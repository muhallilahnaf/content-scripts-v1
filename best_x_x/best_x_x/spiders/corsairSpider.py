import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import corsairItems

class corsairSpider(scrapy.Spider):
    name = "corsair"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 10,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['corsair.com']
    start_urls = [
        'https://www.corsair.com/us/en/Categories/Products/Memory/DOMINATOR-PLATINUM-RGB/p/CMT32GX4M4C3200C16',
        'https://www.corsair.com/us/en/Categories/Products/Memory/Vengeance-PRO-RGB-Black/p/CMW16GX4M2C3200C16',
        'https://www.corsair.com/us/en/Categories/Products/Memory/VENGEANCE-LPX/p/CMK16GX4M2B3200C16',
        'https://www.corsair.com/us/en/Categories/Products/Memory/Vengeance-Series-SODIMM/p/CMSX16GX4M2A2933C19'
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        loader = ItemLoader(item=corsairItems())
        
        title = response.css('.product-name::text').get()
        subtitle = response.css('#product-overview::text').get()
        price = response.css('#price *::text').getall()
        overview = response.css('#panel2 section.panel-template *::text').getall()
        specsAll = response.css('#tab-tech-specs .list-item')
        specs = list()
        for spec in specsAll: 
            specs.append(spec.css('#label::text').get())
            specs.append(spec.css('#value::text').get())


        loader.add_value("title", title)
        loader.add_value("subtitle", subtitle)
        loader.add_value("price", price)
        loader.add_value("specs", specs)
        loader.add_value("overview", overview)

        loadedItems = loader.load_item()

        yield loadedItems
