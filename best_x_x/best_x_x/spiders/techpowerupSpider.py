import scrapy
from scrapy.loader import ItemLoader
from best_x_x.items import techpowerupItems

class techpowerupSpider(scrapy.Spider):
    name = "techpowerup"
    custom_settings = {
        "FEEDS": {
            name + ".json": {
                "format": "json",
                "encoding": "utf8",
                "store_empty": False
            }
        },
        "LOG_LEVEL": "INFO",
        "LOG_FILE": "log_" + name + ".txt",
        "DOWNLOAD_DELAY": 10,
        # "EXTENSIONS": {
        #     'scrapy_files.extensions.sendMail': 100
        # }
    }
    allowed_domains = ['techpowerup.com']
    start_urls = [
        'https://www.techpowerup.com/review/corsair-strafe-rgb-mk-2/'
    ]

    def parse(self, response):
        self.logger.info(f"parse function called on {response.url}")

        options = response.css('#pagesel option')
        for option in options:
            if 'conclusion' in option.css('*::text').get().lower():
                actualLink = option.attrib['value']
        actualLink = f'https://www.techpowerup.com{actualLink}'

        yield scrapy.Request(url=actualLink, callback=self.parseActualLink)

    
    def parseActualLink(self, response):
        self.logger.info(f"parseActualLink function called on {response.url}")

        loader = ItemLoader(item=techpowerupItems())

        title = response.css('header h1 *::text').get()
        pros = response.css('div.plus li *::text').getall()
        cons = response.css('div.minus li *::text').getall()
        verdict = response.css('div.conclusion::text').getall()

        loader.add_value("title", title)
        loader.add_value("verdict", verdict)
        loader.add_value("pros", pros)
        loader.add_value("cons", cons)

        loadedItems = loader.load_item()

        yield loadedItems
