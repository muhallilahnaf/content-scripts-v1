from scrapy import Item, Field
from scrapy.loader.processors import MapCompose, TakeFirst, Join


def removeSpace(text):
    return text.strip()

def removeEmptyString(text):
    return text if text else None


class tomsradarItems(Item):

    title = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )

    subtitle = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )
    
    verdict = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )

    pros = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    cons = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    table = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    article = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join()
    )


class techpowerupItems(Item):

    title = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )
    
    pros = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    cons = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    verdict = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join()
    )


class corsairItems(Item):

    title = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )

    subtitle = Field(
        input_processor=MapCompose(removeSpace),
        output_processor=TakeFirst()
    )
    
    price = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    overview = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

    specs = Field(
        input_processor=MapCompose(removeSpace, removeEmptyString),
        output_processor=Join(' | ')
    )

