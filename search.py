import json
import os
from googleapiclient.discovery import build


def main():

    q = input('enter query: ')
    fq = q.replace(' ', '_')
    exact = input('use exact term? (y/n): ').lower()

    service = build(
        "customsearch", 
        "v1",
        developerKey="AIzaSyCcln29WsMGT8ic7hOmQBzLwAVMOla0gm0"
    )

    if (exact == 'y'):
        req = service.cse().list(
            cx='e5ff063e79d503a33',
            q=q,
            exactTerms=q,
            gl='US',
            lr='lang_en'
        )
    else:
        req = service.cse().list(
            cx='e5ff063e79d503a33',
            q=q,
            gl='US',
            lr='lang_en'
        )

    res = req.execute()

    f1 = open(os.getcwd() + f"/resultFull_{fq}.txt", "w")
    f1.write(json.dumps(res, indent=4))
    print(f"full result dumped in: resultFull_{fq}.txt")
    f1.close()

    results = dict()

    results['totalResults'] = res['searchInformation']['totalResults']
    results['items'] = []

    items = res['items']

    for item in items:
        tmp = dict()
        tmp['title'] = item['title']
        tmp['link'] = item['link']
        tmp['snippet'] = item['snippet']
        results['items'].append(tmp)

    
    f2 = open(f"{os.getcwd()}/result_{fq}.txt", "w")
    f2.write(json.dumps(results, indent=4))
    print(f"simplified results dumped in: result_{fq}.txt")
    f2.close()


if __name__ == '__main__':
    main()